<?php
/*
1. Принимать на вход массив
2. Менять местами четные и нечетные значения,
3. Парами меняет первые 2 элемента, на последние два, потом следующие 2 на предыдущие 2 и т.д.,
если остается 3 элемента массива, меняем соответственно крайние 2,
если остается всего 1 пара чисел - меняем их местами,
если остается 1 элемент - не трогаем
Например:
[1,2,3,4,5,6,7,8,9,10] -> [2,1,4,3,6,5,8,7,10,9] -> [10,9,8,7,6,5,4,3,2,1]
 */

function customSort(array $array): array
{
    $sortArray = sortOddEven($array);
    print_r($sortArray);
    // удаляем центральный элемент массива, если количество элементов нечетное
    if (count($sortArray) & 1) {
        $removeIndex = floor(count($sortArray) / 2);
        $removeItem = array_splice($sortArray, $removeIndex, 1);
    }
    $chunk = array_chunk($sortArray, 2);

    // если остается всего 1 пара чисел - меняем их местами
    if (count($chunk) & 1) {
        $central = floor(count($chunk) / 2);
        array_splice($chunk, $central, 1, [array_reverse($chunk[$central])]);
    }

    $res = array_merge(...array_reverse($chunk));

    if (isset($removeItem)) {
        array_splice($res, $removeIndex, 0, $removeItem);
    }

    return $res;
}

/**
 * Меняем местами четные и нечетные значения массива
 */
function sortOddEven(array $array): array
{
    $res = [];
    while (count($array)) {
        if (count($array) === 1) {
            $res[] = array_shift($array);
            continue;
        }
        list($a, $b) = $array;
        // одно число должно быть четным, другое - нечетным
        if ($a & 1 + $b & 1 === 1) {
            $res = array_merge($res, array_reverse(array_splice($array, 0, 2)));
        } else {
            $res[] = array_shift($array);
        }
    }

    return $res;
}

$array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

echo implode(', ', customSort($array));
